package com.example.thuvu.hddt.network;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by thuvu on 30/07/2017.
 */

public class ServiceBuilder {
    private static final String END_POINT = "https://test.vninvoice.net";
    private static Retrofit retrofit;

    private static Retrofit getRetrofit(){
        if (retrofit==null){
            HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();

            //set your desired log level
            loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

            OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
            //add logging as last interceptor
            httpClient.addInterceptor(loggingInterceptor);
            retrofit = new Retrofit.Builder()
                    .baseUrl(END_POINT)
                    .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(httpClient.build())
                    .build();
        }
        return retrofit;
    }

    public static ApiService getService(){
        return getRetrofit().create(ApiService.class);
    }
}
