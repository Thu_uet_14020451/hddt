package com.example.thuvu.hddt.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatButton;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.thuvu.hddt.R;
import com.example.thuvu.hddt.network.BaseCallBack;
import com.example.thuvu.hddt.network.ServiceBuilder;
import com.example.thuvu.hddt.response.ResponseToken;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG = "thuvu";
    private EditText edtEmail;
    private EditText edtPassword;
    private TextView tvForgotPassword;
    private AppCompatButton btnLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        initViews();
    }

    private void initViews() {
        edtEmail = (EditText) findViewById(R.id.edtEmailLogin);
        edtPassword = (EditText) findViewById(R.id.edtPasswordLogin);
        tvForgotPassword = (TextView) findViewById(R.id.btnLogin);
        btnLogin = (AppCompatButton) findViewById(R.id.btnLogin);

        tvForgotPassword.setOnClickListener(this);
        btnLogin.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tvForgotPassword:
                Intent intent = new Intent(LoginActivity.this, ForgotPasswordActivity.class);
                startActivity(intent);
                break;
            case R.id.btnLogin:
                getToken();
                break;
        }
    }

    public void getToken() {
        ServiceBuilder.getService().getTokenLogin(getString(R.string.content_type), edtEmail.getText().toString(),
                edtPassword.getText().toString())
                .enqueue(new BaseCallBack<List<ResponseToken>>() {
                    @Override
                    public void onResponse(Call<List<ResponseToken>> call, Response<List<ResponseToken>> response) {
                        if (response.isSuccessful()) {
                            Toast.makeText(LoginActivity.this, response.body().get(0).getCode()+"", Toast.LENGTH_SHORT).show();
                            Log.e(TAG, response.body().get(0).getDescription());
                            Toast.makeText(LoginActivity.this, "success", Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(LoginActivity.this, response.body().get(1).getField(), Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<List<ResponseToken>> call, Throwable t) {
                        super.onFailure(call, t);
                        Toast.makeText(LoginActivity.this, "ERROR: fail", Toast.LENGTH_SHORT).show();

                    }
                });
    }
}
