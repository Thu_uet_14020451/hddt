package com.example.thuvu.hddt.response;

import java.io.Serializable;

/**
 * Created by thuvu on 30/07/2017.
 */

public class ResponseToken implements Serializable {
    private Integer code;
    private String field;
    private String description;

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getField() {
        return field;
    }

    public void setField(String field) {
        this.field = field;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
