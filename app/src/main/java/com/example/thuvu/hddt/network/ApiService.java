package com.example.thuvu.hddt.network;

import com.example.thuvu.hddt.response.ResponseToken;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by thuvu on 30/07/2017.
 */

public interface ApiService {

    @POST("/token")
    Call<List<ResponseToken>> getTokenLogin(@Header("Content-Type") String content_type,
                                            @Query("userName") String userName,
                                            @Query("password") String password);
}
