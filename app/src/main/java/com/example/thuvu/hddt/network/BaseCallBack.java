package com.example.thuvu.hddt.network;

import android.util.Log;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by thuvu on 30/07/2017.
 */

public abstract class BaseCallBack<T> implements Callback<T> {
    @Override
    public void onResponse(Call<T> call, Response<T> response) {
        Log.e("TAGG",response.toString());
        Log.e("TAGG", call.toString());
        if (response.isSuccessful()) {
            Log.d("TAG", "success: " + response.message());
        }
    }

    @Override
    public void onFailure(Call<T> call, Throwable t) {
        Log.e("TAGG---error",t.toString());
    }
}
